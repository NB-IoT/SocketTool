﻿using System;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace SocketTool
{
    public partial class Create_Socket_Server_Form : Form
    {
        public static string port;
        private TextBox socketLinkInfo;
        private TextBox sendAndReceiveInfo;
        private TextBox clientIP;
        public static Socket udpServerSocket;
        private const int INIT_PORT = 0;
        private const int INIT_CAPACITY = 1024;
        private const int INIT_THREAD_SLEEP_TIME = 1000;
        private static IPEndPoint serverIP = null;

        public Create_Socket_Server_Form(TextBox socketLinkInfo, TextBox sendAndReceiveInfo, TextBox clientIP)
        {
            this.socketLinkInfo = socketLinkInfo;
            this.sendAndReceiveInfo = sendAndReceiveInfo;
            this.clientIP = clientIP;
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            socketLinkInfo.AppendText("服务器端口号为：" + getPort() + "\r\n");
            this.Close();
            createUDPSocketServer();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        public String getPort() {
            port = textBox1.Text.ToString();
            return port;
        }

        private void Create_Socket_Server_Load(object sender, EventArgs e)
        {

        }

        private void createUDPSocketServer() {
            serverIP = new IPEndPoint(0, int.Parse(port));
            UDPServer(serverIP);
        }

        private void UDPServer(IPEndPoint serverIP)
        {
            System.Console.WriteLine(serverIP.Address);
            socketLinkInfo.AppendText("UDP服务器开始监听" + serverIP.Port + "端口......\r\n");
            udpServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            udpServerSocket.Bind(serverIP);
            //接收和发送消息
            receiveAndSendMsg(serverIP);
        }

        private void receiveAndSendMsg(IPEndPoint serverIP) {
            Control.CheckForIllegalCrossThreadCalls = false;
            //开启接收消息线程
            Thread t = new Thread(ReciveMsg);
            t.Start();
            //开启发送消息线程
            Thread t2 = new Thread(sendMsg);
            t2.Start();
        }

        /// <summary>
        /// 向特定ip的主机的端口发送数据报
        /// </summary>
        void sendMsg()
        {
            EndPoint point = new IPEndPoint(IPAddress.Parse(getLocalIP()), int.Parse(port));
            while (ServerForm.flag)
            {
                Thread.Sleep(INIT_THREAD_SLEEP_TIME);
                string msg ="发送";
                sendAndReceiveInfo.AppendText("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "]客户端发送消息:" + msg + "\r\n");
                udpServerSocket.SendTo(Encoding.UTF8.GetBytes(msg), point);   
            }

            if(udpServerSocket != null)
            {
                udpServerSocket.Close();
            }
            
        }
        /// <summary>
        /// 接收发送给本机ip对应端口号的数据报
        /// </summary>
        void ReciveMsg()
        {
            clientIP.Clear();
            Boolean flag = true;
            //用来保存发送方的ip和端口号
            EndPoint point = new IPEndPoint(IPAddress.Any, INIT_PORT);
            byte[] buffer = new byte[INIT_CAPACITY];
            while (ServerForm.flag)
            {
                //接收数据报
                try
                {
                    int length = udpServerSocket.ReceiveFrom(buffer, ref point);
                    if (flag)
                    {
                        clientIP.AppendText(point.ToString());
                        flag = false;
                    }
                    string message = Encoding.UTF8.GetString(buffer, 0, length);
                    sendAndReceiveInfo.AppendText("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "]服务端接收消息:" + message + "\r\n");
                    sendAndReceiveInfo.AppendText("***************************************************************\r\n");
                }
                catch (Exception ex) {
                    break;
                }
                
            }

        }

        /// <summary>  
        /// 获取当前使用的IP  
        /// </summary>  
        /// <returns></returns>  
        public static string getLocalIP()
        {
            string result = RunApp("route", "print", true);
            Match m = Regex.Match(result, @"0.0.0.0\s+0.0.0.0\s+(\d+.\d+.\d+.\d+)\s+(\d+.\d+.\d+.\d+)");
            if (m.Success)
            {
                return m.Groups[2].Value;
            }
            else
            {
                try
                {
                    TcpClient c = new TcpClient();
                    c.Connect("www.baidu.com", 80);
                    string ip = ((IPEndPoint)c.Client.LocalEndPoint).Address.ToString();
                    c.Close();
                    return ip;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        /// <summary>  
        /// 获取本机主DNS  
        /// </summary>  
        /// <returns></returns>  
        public static string GetPrimaryDNS()
        {
            string result = RunApp("nslookup", "", true);
            Match m = Regex.Match(result, @"\d+\.\d+\.\d+\.\d+");
            if (m.Success)
            {
                return m.Value;
            }
            else
            {
                return null;
            }
        }

        /// <summary>  
        /// 运行一个控制台程序并返回其输出参数。  
        /// </summary>  
        /// <param name="filename">程序名</param>  
        /// <param name="arguments">输入参数</param>  
        /// <returns></returns>  
        public static string RunApp(string filename, string arguments, bool recordLog)
        {
            try
            {
                if (recordLog)
                {
                    Trace.WriteLine(filename + " " + arguments);
                }
                Process proc = new Process();
                proc.StartInfo.FileName = filename;
                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.Arguments = arguments;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.UseShellExecute = false;
                proc.Start();

                using (System.IO.StreamReader sr = new System.IO.StreamReader(proc.StandardOutput.BaseStream, Encoding.Default))
                { 
                    Thread.Sleep(100);        
                                           
                    if (!proc.HasExited)    
                    {                      
                        proc.Kill();
                    }
                    string txt = sr.ReadToEnd();
                    sr.Close();
                    if (recordLog)
                        Trace.WriteLine(txt);
                    return txt;
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return ex.Message;
            }
        }
    }
}
