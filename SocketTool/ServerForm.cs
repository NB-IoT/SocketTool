﻿using System;
using System.Windows.Forms;

namespace SocketTool
{
    public partial class ServerForm : Form
    {
        public static Boolean flag = true;
        public ServerForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Create_Socket_Server_Form.udpServerSocket != null)
            {
                socketLinkInfo.AppendText("*********************UDP连接已断开*********************\r\n");
                flag = false;
                newBuildButton.Enabled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)      //点击退出，关闭窗口
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            flag = true;
            Create_Socket_Server_Form create_Socket_Server_Form = new Create_Socket_Server_Form(socketLinkInfo, sendAndReceiveInfo, clientIP);
            create_Socket_Server_Form.ShowDialog();
            newBuildButton.Enabled = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void clearScoketLinkInfo_Click(object sender, EventArgs e)
        {
            socketLinkInfo.Clear();
        }

        private void clearReceiveAndSendInfo_Click(object sender, EventArgs e)
        {
            sendAndReceiveInfo.Clear();
        }
    }
}
