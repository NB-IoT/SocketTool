﻿namespace SocketTool
{
    partial class ServerForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.newBuildButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.sendAndReceiveInfo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.clientIP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.socketLinkInfo = new System.Windows.Forms.TextBox();
            this.clearScoketLinkInfo = new System.Windows.Forms.Button();
            this.clearReceiveAndSendInfo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // newBuildButton
            // 
            this.newBuildButton.Location = new System.Drawing.Point(12, 12);
            this.newBuildButton.Name = "newBuildButton";
            this.newBuildButton.Size = new System.Drawing.Size(84, 32);
            this.newBuildButton.TabIndex = 0;
            this.newBuildButton.Text = "创建";
            this.newBuildButton.UseVisualStyleBackColor = true;
            this.newBuildButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(109, 12);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(84, 32);
            this.deleteButton.TabIndex = 1;
            this.deleteButton.Text = "断开";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(206, 12);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(84, 32);
            this.exitButton.TabIndex = 2;
            this.exitButton.Text = "退出";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // sendAndReceiveInfo
            // 
            this.sendAndReceiveInfo.Location = new System.Drawing.Point(12, 254);
            this.sendAndReceiveInfo.Multiline = true;
            this.sendAndReceiveInfo.Name = "sendAndReceiveInfo";
            this.sendAndReceiveInfo.ReadOnly = true;
            this.sendAndReceiveInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sendAndReceiveInfo.Size = new System.Drawing.Size(548, 159);
            this.sendAndReceiveInfo.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 10F);
            this.label1.Location = new System.Drawing.Point(14, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "socket连接信息";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 10F);
            this.label2.Location = new System.Drawing.Point(14, 238);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "接收与发送消息";
            // 
            // clientIP
            // 
            this.clientIP.Enabled = false;
            this.clientIP.Font = new System.Drawing.Font("宋体", 15F);
            this.clientIP.Location = new System.Drawing.Point(456, 12);
            this.clientIP.Multiline = true;
            this.clientIP.Name = "clientIP";
            this.clientIP.Size = new System.Drawing.Size(186, 32);
            this.clientIP.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 10F);
            this.label3.Location = new System.Drawing.Point(387, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 8;
            this.label3.Text = "客户端IP";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // socketLinkInfo
            // 
            this.socketLinkInfo.Location = new System.Drawing.Point(12, 74);
            this.socketLinkInfo.Multiline = true;
            this.socketLinkInfo.Name = "socketLinkInfo";
            this.socketLinkInfo.ReadOnly = true;
            this.socketLinkInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.socketLinkInfo.Size = new System.Drawing.Size(548, 159);
            this.socketLinkInfo.TabIndex = 3;
            this.socketLinkInfo.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // clearScoketLinkInfo
            // 
            this.clearScoketLinkInfo.Location = new System.Drawing.Point(566, 72);
            this.clearScoketLinkInfo.Name = "clearScoketLinkInfo";
            this.clearScoketLinkInfo.Size = new System.Drawing.Size(75, 23);
            this.clearScoketLinkInfo.TabIndex = 9;
            this.clearScoketLinkInfo.Text = "清除";
            this.clearScoketLinkInfo.UseVisualStyleBackColor = true;
            this.clearScoketLinkInfo.Click += new System.EventHandler(this.clearScoketLinkInfo_Click);
            // 
            // clearReceiveAndSendInfo
            // 
            this.clearReceiveAndSendInfo.Location = new System.Drawing.Point(566, 254);
            this.clearReceiveAndSendInfo.Name = "clearReceiveAndSendInfo";
            this.clearReceiveAndSendInfo.Size = new System.Drawing.Size(75, 23);
            this.clearReceiveAndSendInfo.TabIndex = 10;
            this.clearReceiveAndSendInfo.Text = "清除";
            this.clearReceiveAndSendInfo.UseVisualStyleBackColor = true;
            this.clearReceiveAndSendInfo.Click += new System.EventHandler(this.clearReceiveAndSendInfo_Click);
            // 
            // ServerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 424);
            this.Controls.Add(this.clearReceiveAndSendInfo);
            this.Controls.Add(this.clearScoketLinkInfo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.clientIP);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sendAndReceiveInfo);
            this.Controls.Add(this.socketLinkInfo);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.newBuildButton);
            this.Name = "ServerForm";
            this.Text = "UDP Socket 调试工具 V1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button newBuildButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.TextBox sendAndReceiveInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox clientIP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox socketLinkInfo;
        private System.Windows.Forms.Button clearScoketLinkInfo;
        private System.Windows.Forms.Button clearReceiveAndSendInfo;
    }
}

